#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<'EOUSAGE'
Usage:	run-clippy.sh [-c cargo] [-n]
	-c	specify the Cargo command to use
	-n	also warn about lints in the Clippy "nursery" category
EOUSAGE
}

unset run_nursery
cargo='cargo'

while getopts 'c:n' o; do
	case "$o" in
		c)
			cargo="$OPTARG"
			;;

		n)
			run_nursery=1
			;;

		*)
			usage 1>&2
			exit 1
			;;
	esac
done

# The Clippy lint selection is synced with Rust 1.72.0
run_clippy() {
	"$cargo" clippy \
		"$@" \
		-- \
		-W warnings \
		-W future-incompatible \
		-W nonstandard-style \
		-W rust-2018-compatibility \
		-W rust-2018-idioms \
		-W rust-2021-compatibility \
		-W unused \
		-W clippy::cargo \
		-W clippy::complexity \
		-W clippy::correctness \
		-W clippy::pedantic \
		-W clippy::perf \
		-W clippy::pedantic \
		-W clippy::restriction \
			-A clippy::implicit_return \
			-A clippy::question_mark_used \
			-A clippy::ref_patterns \
			-A clippy::single_call_fn \
			-A clippy::semicolon_outside_block \
			-A clippy::separated_literal_suffix \
			-A clippy::std_instead_of_core \
		-W clippy::style \
		-W clippy::suspicious \
			-A clippy::blanket_clippy_restriction_lints \
		${run_nursery+-W clippy::nursery}
}

run_clippy
run_clippy --tests
