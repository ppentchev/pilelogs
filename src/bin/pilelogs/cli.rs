//! Parse the command-line parameters for the opinionated syslog daemon.
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause

use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use clap::{Parser, Subcommand};
use tracing::Level;

/// The parameters passed to the "listen" subcommand.
#[derive(Debug)]
pub struct ListenArgs {
    /// The path to the file to write the messages to.
    filename: PathBuf,

    /// The path to the Unix-domain socket to listen on.
    socket: PathBuf,

    /// Verbose output: log some diagnostic messages.
    verbose: bool,

    /// Debug output: log lots more diagnostics.
    debug: bool,
}

impl ListenArgs {
    /// Get the path to the file to write to.
    pub fn filename(&self) -> &Path {
        &self.filename
    }

    /// Get the path to the socket to listen on.
    pub fn socket(&self) -> &Path {
        &self.socket
    }

    /// Get the logging level configured by the "--debug" and "--verbose" options.
    pub const fn log_level(&self) -> Level {
        if self.debug {
            Level::TRACE
        } else if self.verbose {
            Level::DEBUG
        } else {
            Level::INFO
        }
    }
}

/// The top-level operating mode for the pilelogs server.
#[derive(Debug)]
pub enum Mode {
    /// Listen for incoming messages, log them to a file.
    Listen(ListenArgs),
}

/// The top-level subcommands.
#[derive(Debug, Subcommand)]
enum CliCommand {
    /// Listen for incoming messages, log them to a file.
    Listen {
        /// The path to the file to write the messages to.
        #[clap(default_value("/var/log/debug"), short, long)]
        filename: PathBuf,

        /// The path to the Unix-domain socket to listen on.
        #[clap(default_value("/dev/log"), short('u'), long)]
        socket: PathBuf,

        /// Verbose output: log some diagnostic messages.
        #[clap(short, long)]
        verbose: bool,

        /// Debug output: log lots more diagnostics.
        #[clap(short, long)]
        debug: bool,
    },
}

/// The top-level command-line parsing structure.
#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct Cli {
    /// The subcommand specified, what to do.
    #[clap(subcommand)]
    command: CliCommand,
}

/// Parse the command-line arguments, determine what to do.
pub fn parse_args() -> Result<Mode> {
    let top = Cli::try_parse().context("Could not parse the command-line arguments")?;
    match top.command {
        CliCommand::Listen {
            filename,
            socket,
            verbose,
            debug,
        } => Ok(Mode::Listen(ListenArgs {
            filename,
            socket,
            verbose,
            debug,
        })),
    }
}
