//! A couple of tests for the "log everything to a file" daemon.
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause

use std::env;
use std::fs::{File, OpenOptions};
use std::io::{BufRead, BufReader, ErrorKind};
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::net::UnixDatagram;
use std::path::{Path, PathBuf};
use std::process::{Child, Command};
use std::thread;
use std::time::Duration;

use anyhow::{anyhow, bail, Context, Result};
use syslog::{Facility, Formatter3164};
use tracing::{info, trace};
use tracing_test::traced_test;

/// Determine the path to the specified program.
fn get_exe_path(name: &str) -> Result<PathBuf> {
    let current = env::current_exe()?;
    let exe_dir = {
        let basedir = Path::new(&current).parent().with_context(|| {
            format!(
                "Could not get the parent directory of {current}",
                current = current.display()
            )
        })?;
        if basedir.file_name().with_context(|| {
            format!(
                "Could not get the base name of {basedir}",
                basedir = basedir.display()
            )
        })? == "deps"
        {
            basedir.parent().with_context(|| {
                format!(
                    "Could not get the parent directory of {basedir}",
                    basedir = basedir.display()
                )
            })?
        } else {
            basedir
        }
    };
    Ok(exe_dir.join(name))
}

/// Wait for the socket to appear and stuff.
fn wait_and_connect<P: AsRef<Path>>(
    sock_path: P,
    sock_path_str: &str,
    child: &mut Child,
) -> Result<UnixDatagram> {
    let wait_time = Duration::from_millis(100);
    for _ in 0_u32..10_u32 {
        info!("Trying to connect to {sock_path_str}");
        let sock = UnixDatagram::unbound()
            .context("Could not create an unbound datagram socket to connect on")?;
        match sock.connect(&sock_path) {
            Ok(()) => {
                info!("Connected!");
                trace!("{sock:?}");
                return Ok(sock);
            }
            Err(err)
                if err.kind() == ErrorKind::NotFound
                    || err.kind() == ErrorKind::ConnectionRefused => {}
            Err(err) => {
                return Err(err)
                    .with_context(|| format!("Could not connect to the {sock_path_str} socket"));
            }
        }

        if let Some(rc) = child.try_wait().with_context(|| {
            format!(
                "Could not examine the status of the pilelogs child {pid}",
                pid = child.id()
            )
        })? {
            bail!(
                "The pilelogs child {pid} exited with code {rc} before creating the log socket",
                pid = child.id()
            );
        }
        info!("Waiting for a while...");
        thread::sleep(wait_time);
    }
    bail!(
        "The pilelogs child {pid} did not create the log socket in time",
        pid = child.id()
    );
}

fn wait_and_read<B: BufRead>(debug_rdr: &mut B, debug_path_str: &str) -> Result<String> {
    let wait_time = Duration::from_millis(100);
    let mut line = String::new();
    for _ in 0_u32..10_u32 {
        let nread = debug_rdr
            .read_line(&mut line)
            .with_context(|| format!("Could not read from {debug_path_str}"))?;
        if nread > 0 {
            trace!("{nread} {line:?}");
            return Ok(line.trim_end_matches('\n').to_owned());
        }
        info!("Waiting for a while...");
        thread::sleep(wait_time);
    }
    bail!("The child process did not write to the logfile in time");
}

fn wait_and_check<B, F>(
    debug_rdr: &mut B,
    debug_path_str: &str,
    check_line: F,
    tag: &str,
) -> Result<()>
where
    B: BufRead,
    F: FnOnce(&str) -> bool,
{
    info!("About to read '{tag}' from the logfile");
    let line = wait_and_read(debug_rdr, debug_path_str)
        .context("Could not read from the debug logfile")?;
    trace!("{line:?}");
    if !check_line(&line) {
        bail!("Expected a '{tag}' line: {line}");
    }
    info!("Got the '{tag}' line");
    Ok(())
}

fn init_debug_log<P: AsRef<Path>>(debug_path: P, debug_path_str: &str) -> Result<impl BufRead> {
    {
        let debug_file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .mode(0o600)
            .open(&debug_path)
            .with_context(|| format!("Could not create the {debug_path_str} file"))?;
        trace!("{debug_file:?}");
    }
    let debug_rdr = {
        let debug_file = File::open(&debug_path)
            .with_context(|| format!("Could not reopen {debug_path_str}"))?;
        BufReader::new(debug_file)
    };
    trace!("{debug_rdr:?}");
    Ok(debug_rdr)
}

/// Wait for the `pilelogs` child process to die, examine its exit status.
fn kill_child(mut child: Child) -> Result<()> {
    info!("Killing the {pid} pilelogs child", pid = child.id());
    child.kill().context("Could not kill the pilelogs child")?;
    let stat = child
        .wait()
        .context("Could not examine the exit status of the pilelogs child")?;
    trace!("{stat:?}");
    Ok(())
}

/// Create a temporary log socket and logfile, run `pilelogs`.
#[test]
#[traced_test]
fn run_with_temp_socket() -> Result<()> {
    #[allow(clippy::print_stdout)]
    {
        println!();
    }

    let tempd = tempfile::tempdir()?;
    info!(
        "Using {tempd} as a temporary directory",
        tempd = tempd.path().display()
    );

    let debug_path = tempd.path().join("debug.log");
    let debug_path_str = debug_path
        .to_str()
        .with_context(|| format!("Could not convert {debug_path:?} to a string"))?;
    let mut debug_rdr = init_debug_log(&debug_path, debug_path_str)?;

    let sock_path = tempd.path().join("log.sock");
    let sock_path_str = sock_path
        .to_str()
        .with_context(|| format!("Could not convert {sock_path:?} to a string"))?;
    if sock_path.exists() {
        bail!("Did not expect {sock_path_str} to exist");
    }

    let prog_path = get_exe_path("pilelogs")
        .context("Could not determine the path to the pilelogs executable")?;
    if !prog_path.is_file() {
        bail!(
            "Expected {prog_path} to be a regular file",
            prog_path = prog_path.display()
        )
    }
    info!("About to test {prog_path}", prog_path = prog_path.display());

    let mut child = Command::new(&prog_path)
        .args(["listen", "-u", sock_path_str, "-f", debug_path_str, "-d"])
        .spawn()
        .with_context(|| {
            format!(
                "Could not execute {prog_path}",
                prog_path = prog_path.display()
            )
        })?;
    info!("Spawned child process {pid}", pid = child.id());

    let res = run_tests_with_temp_socket(
        &sock_path,
        sock_path_str,
        &mut child,
        &mut debug_rdr,
        debug_path_str,
    );

    let proc_res = child
        .try_wait()
        .with_context(|| {
            format!(
                "Could not check whether the {pid} pilelogs child is done",
                pid = child.id()
            )
        })
        .and_then(|wres| match wres {
            None => kill_child(child),
            Some(stat) => {
                info!("The {pid} pilelogs child is already dead", pid = child.id());
                trace!("{stat:?}");
                Ok(())
            }
        });

    res.and(proc_res)
}

fn run_tests_with_temp_socket<PS, B>(
    sock_path: PS,
    sock_path_str: &str,
    child: &mut Child,
    debug_rdr: &mut B,
    debug_path_str: &str,
) -> Result<()>
where
    PS: AsRef<Path>,
    B: BufRead,
{
    wait_and_check(
        debug_rdr,
        debug_path_str,
        |line| line.contains(" pilelogs[") && line.ends_with("]: starting"),
        "pilelogs starting",
    )?;

    let sock = wait_and_connect(&sock_path, sock_path_str, child)
        .context("Could not connect to the log socket")?;
    trace!("{sock:?}");

    {
        info!("About to say hello");
        let written = sock
            .send(b"hello\n")
            .with_context(|| format!("Could not write to the {sock_path_str} socket"))?;
        trace!(written);
    }

    wait_and_check(
        debug_rdr,
        debug_path_str,
        |line| line.contains("Could not parse a syslog message") && line.contains("hello"),
        "invalid syslog message hello",
    )?;

    {
        info!("About to run 'logger'");
        let logger_status = Command::new("logger")
            .args(["-u", sock_path_str, "--", "hello"])
            .status()
            .context("Could not run 'logger'")?;
        if !logger_status.success() {
            bail!("'logger hello' failed: {logger_status:?}");
        }
    }

    wait_and_check(
        debug_rdr,
        debug_path_str,
        |line| line.ends_with(": hello"),
        "hello",
    )?;

    {
        info!("About to run 'logger' with a process ID");
        let logger_status = Command::new("logger")
            .args(["-u", sock_path_str, "-i", "--", "hello there"])
            .status()
            .context("Could not run 'logger'")?;
        if !logger_status.success() {
            bail!("'logger hello' failed: {logger_status:?}");
        }
    }

    wait_and_check(
        debug_rdr,
        debug_path_str,
        |line| line.ends_with(": hello there"),
        "hello there",
    )?;

    {
        info!("About to connect to the Unix-domain socket");
        let mut logger = syslog::unix_custom(
            Formatter3164 {
                facility: Facility::LOG_USER,
                hostname: Some("neverland".to_owned()),
                process: "loggish".to_owned(),
                pid: 616,
            },
            &sock_path,
        )
        .map_err(|err| anyhow!("Could not create an RFC3164 logger: {err}"))?;
        logger
            .debug("this is a test")
            .map_err(|err| anyhow!("Could not log a line using the RFC3164 logger: {err}"))?;
    }

    wait_and_check(
        debug_rdr,
        debug_path_str,
        |line| line.ends_with(" neverland loggish[616]: this is a test"),
        "this is a test",
    )?;

    Ok(())
}
