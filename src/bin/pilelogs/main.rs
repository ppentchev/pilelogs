//! Listen for syslog messages, write them all to a single file.
//!
//! This is an extremely opinionated syslog daemon.
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause

use std::fs::{self, File, OpenOptions, Permissions};
use std::io::{ErrorKind, LineWriter, Write};
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::net::UnixDatagram;
use std::path::Path;
use std::process;

use anyhow::{bail, Context, Result};
use chrono::{DateTime, FixedOffset, Utc};
use encoding::all::{ISO_8859_15, UTF_8};
use encoding::{DecoderTrap, Encoding};
use nix::unistd;
use syslog_loose::Variant;
use tokio::join;
use tokio::signal::unix::{self as unix_signal, SignalKind};
use tokio::sync::mpsc::{self, Sender};
use tracing::{debug, error, info, trace};
use tracing_subscriber::FmtSubscriber;

mod cli;

use crate::cli::{ListenArgs, Mode};

#[cfg(test)]
mod tests;

/// What to do in the outer writer loop.
#[derive(Debug)]
enum Loopy {
    /// Once more into the breach.
    Continue,

    /// Let's blow this popsicle stand.
    Done,
}

/// Messages sent from the worker threads to the main one.
#[derive(Debug)]
enum MainMsg {
    /// Received a datagram on the syslog Unix-domain socket.
    Received(Vec<u8>),

    /// Failed to receive a message from the Unix-domain socket.
    ReceiveError(String),

    /// Received a SIGHUP signal.
    SignalHangup,

    /// Failed to register a signal handler or await a signal.
    SignalError(String),
}

/// Format a timestamp the way syslog traditionally does.
///
/// Example: "Sep  6 03:14:15"
fn format_tstamp(tstamp: DateTime<FixedOffset>) -> String {
    tstamp.format("%b %e %H:%M:%S").to_string()
}

/// Parse a syslog datagram, return a string to be written to the output file.
fn parse_message(raw: &str, sys_hostname: &str) -> Result<String> {
    trace!(raw);
    let msg = syslog_loose::parse_message(raw, Variant::Either);
    trace!("{msg:?}");
    match msg.timestamp {
        None => bail!("Not an RFC3164 or RFC5424 datagram: {msg:?}"),
        Some(tstamp) => {
            let tstamp_str = format_tstamp(tstamp);
            let (hostname, app) = match msg.hostname {
                Some(hostname) => msg.appname.map_or_else(
                    || (sys_hostname.to_owned(), format!(" {hostname}")),
                    |app| (hostname.to_owned(), format!(" {app}")),
                ),
                None => msg.appname.map_or_else(
                    || (sys_hostname.to_owned(), String::new()),
                    |app| (sys_hostname.to_owned(), format!(" {app}")),
                ),
            };
            let pid = msg
                .procid
                .map_or_else(String::new, |pid| format!("[{pid}]"));
            Ok(format!(
                "{tstamp_str} {hostname}{app}{pid}: {text}\n",
                text = msg.msg
            ))
        }
    }
}

/// Log a parsed and formatted message line to the output file.
#[allow(clippy::use_debug)]
fn log_line<W: Write>(writer: &mut LineWriter<W>, line: &str) {
    if let Err(err) = writer.write_all(line.as_bytes()) {
        error!("Could not write {line:?} to the logfile: {err}");
    }
}

/// Log an invalid datagram to the output file.
fn log_invalid<W: Write, F: FnOnce() -> String>(writer: &mut LineWriter<W>, dgram: &[u8], errf: F) {
    let line = format!("Invalid datagram: {err}: {dgram:?}\n", err = errf());
    log_line(writer, &line);
}

/// Listen on the Unix-domain socket, send received datagrams to the main thread.
async fn socket_listen(tx: Sender<MainMsg>, sock: UnixDatagram) {
    debug!("socket_listen() starting");
    loop {
        let mut buf: [u8; 4096] = [0; 4096];
        let nread = match sock.recv(&mut buf[..]) {
            Ok(nread) => nread,
            Err(recv_err) => {
                if tx
                    .send(MainMsg::ReceiveError(format!("{recv_err}")))
                    .await
                    .is_err()
                {
                    error!("Could not tell the main thread about a receive error: {recv_err}");
                }
                break;
            }
        };

        #[allow(clippy::indexing_slicing)]
        let dgram = buf[..nread].into();
        trace!("{nread} {dgram:?}");
        if tx.send(MainMsg::Received(dgram)).await.is_err() {
            error!("Could not tell the main thread that we received a message");
            break;
        }
    }
}

/// Parse a single syslog message, format its contents into a line of text.
fn process_datagram<W: Write>(dgram: &[u8], hostname: &str, writer: &mut LineWriter<W>) {
    match decode_to_string(dgram) {
        Ok(u8str) => {
            trace!(u8str);
            match parse_message(&u8str, hostname) {
                Ok(msg) => {
                    trace!(msg);
                    log_line(writer, &msg);
                }
                Err(parse_err) => {
                    trace!("{parse_err:?}");
                    log_invalid(writer, dgram, || {
                        format!("Could not parse a syslog message: {parse_err}")
                    });
                }
            }
        }
        Err(u8err) => {
            trace!("{u8err:?}");
            log_invalid(writer, dgram, || {
                format!("Could not convert to a text string: {u8err}")
            });
        }
    }
}

/// Wait for a SIGHUP, send a message to the main thread.
async fn signal_loop(tx: Sender<MainMsg>) {
    debug!("signal_loop() starting");
    let mut channel = match unix_signal::signal(SignalKind::hangup()) {
        Ok(chan) => chan,
        Err(err) => {
            if tx
                .send(MainMsg::SignalError(format!(
                    "Could not register the signal handler: {err}"
                )))
                .await
                .is_err()
            {
                error!(
                    "Could not tell the main thread that we could not register the handler: {err}"
                );
            }
            return;
        }
    };
    loop {
        if channel.recv().await.is_none() {
            if tx
                .send(MainMsg::SignalError("No more signals".to_owned()))
                .await
                .is_err()
            {
                error!("Could not tell the main thread 'no more signals'");
            }
            return;
        }

        if tx.send(MainMsg::SignalHangup).await.is_err() {
            error!("Could not tell the main thread that we got a signal");
            return;
        }
    }
}

/// Open the output logfile for writing.
fn get_writer<P: AsRef<Path>>(filename: P, filename_str: &str) -> Result<LineWriter<File>> {
    let outfile = OpenOptions::new()
        .append(true)
        .create(true)
        .open(filename)
        .with_context(|| format!("Could not open the {filename_str} output logfile"))?;
    trace!("{outfile:?}");
    Ok(LineWriter::new(outfile))
}

/// Decode a sequence of bytes, trying UTF-8 first, falling back to ISO-8859-15.
fn decode_to_string(raw: &[u8]) -> Result<String> {
    if let Ok(res) = UTF_8.decode(raw, DecoderTrap::Strict) {
        return Ok(res);
    }
    if let Ok(res) = ISO_8859_15.decode(raw, DecoderTrap::Strict) {
        return Ok(res);
    }
    bail!("Could not decode as either UTF-8 or ISO-8859-15: {raw:?}");
}

/// Listen on the Unix-domain socket, write messages to a file.
///
/// Should not return!
async fn cmd_listen(args: &ListenArgs) -> Result<()> {
    let hostname = decode_to_string(
        unistd::gethostname()
            .context("Could not obtain the system hostname")?
            .as_bytes(),
    )
    .context("Could not parse the system hostname as a string")?;

    let filename_str = args.filename().to_str().with_context(|| {
        format!(
            "Could not convert {filename:?} to a string",
            filename = args.filename()
        )
    })?;

    let socket_str = args.socket().to_str().with_context(|| {
        format!(
            "Could not convert {socket:?} to a string",
            socket = args.socket()
        )
    })?;
    info!("Removing {socket_str} if it already exists");
    if let Err(err) = fs::remove_file(args.socket()) {
        if err.kind() != ErrorKind::NotFound {
            return Err(err).with_context(|| format!("Could not remove {socket_str}"));
        }
    }
    info!("Binding to {socket_str}");
    let sock = UnixDatagram::bind(args.socket())
        .with_context(|| format!("Could not create or listen on the {socket_str} socket"))?;
    trace!("{sock:?}");
    info!("Making sure {socket_str} is world-writable");
    fs::set_permissions(args.socket(), Permissions::from_mode(0o666))
        .with_context(|| format!("Could not make {socket_str} world-writable"))?;

    info!("Setting up the message queue");
    let (mut rx, socket_task, signal_task) = {
        let (tx, rx) = mpsc::channel(128);

        debug!("Spawning the socket listening task");
        let socket_task = {
            let tx_socket = tx.clone();
            tokio::spawn(async move { socket_listen(tx_socket, sock).await })
        };
        trace!("{socket_task:?}");

        debug!("Spawning the signal handling task");
        let signal_task = tokio::spawn(async move { signal_loop(tx).await });
        trace!("{signal_task:?}");

        (rx, socket_task, signal_task)
    };

    // An outer loop to (re)open the logfile, and an inner loop to process
    // incoming syslog messages and signals.
    info!("Waiting for incoming messages");
    loop {
        info!("Opening {filename_str} for output");
        let mut writer = get_writer(args.filename(), filename_str)?;
        {
            let current_time = Utc::now().fixed_offset();
            let init_msg = format!(
                "{tstamp} {hostname} pilelogs[{pid}]: starting\n",
                tstamp = format_tstamp(current_time),
                pid = process::id()
            );
            log_line(&mut writer, &init_msg);
        }

        let more = loop {
            match rx.recv().await {
                None => {
                    error!("All our senders are dead?");
                    break Loopy::Done;
                }
                Some(msg) => {
                    trace!("{msg:?}");
                    match msg {
                        MainMsg::Received(dgram) => {
                            process_datagram(&dgram, &hostname, &mut writer);
                        }
                        MainMsg::ReceiveError(err) => {
                            error!("The socket thread reported an error: {err}");
                        }
                        MainMsg::SignalHangup => {
                            info!("Received SIGHUP, trying to reopen the output file");
                            break Loopy::Continue;
                        }
                        MainMsg::SignalError(err) => {
                            error!("The signal thread reported an error: {err}");
                        }
                    }
                }
            }
        };
        if matches!(more, Loopy::Done) {
            info!("Like tears in the rain...");
            break;
        }
    }

    let socket_res = join!(socket_task);
    trace!("socket task result: {socket_res:?}");
    let signal_res = join!(signal_task);
    trace!("signal task result: {signal_res:?}");
    bail!("All our senders are dead");
}

/// Initialize the logging subsystem.
fn setup_tracing(mode: &Mode) -> Result<()> {
    let level = match *mode {
        Mode::Listen(ref args) => args.log_level(),
    };
    let formatter = FmtSubscriber::builder().with_max_level(level).finish();
    tracing::subscriber::set_global_default(formatter).context("tracing init")?;
    Ok(())
}

/// Parse the command-line arguments, listen for messages, log them to a file.
#[tokio::main]
async fn main() -> Result<()> {
    let mode = cli::parse_args()?;
    setup_tracing(&mode).context("Could not initialize the tracing subsystem")?;
    trace!("{mode:?}");
    match mode {
        Mode::Listen(args) => cmd_listen(&args).await,
    }
}
