<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# pilelogs - log syslog messages to a single file

The `pilelogs` program is an extremely opinionated syslog daemon that logs all
messages to a single file, `/var/log/debug` by default.

## Usage

The `pilelogs` program currently accepts a single subcommand, `listen`, with
the following command-line options:

- `--debug`, `-d`: display lots of diagnostic information; overrides `--verbose`
- `--filename`, `-f`: specify the path to the file to log the messages to
  (default: `/var/log/debug`)
- `--socket`, `-u`: specify the path to the Unix-domain socket to listen on
  (default: `/dev/log`)
- `--verbose`, `-v`: display some diagnostic messages

## Rotating the logfile

If the `pilelogs` daemon receives a `SIGHUP` signal, it will close the logfile
that it is currently writing to and open the file with the specified name once
again.
This gives tools like `logrotate` the chance to move the old logfile away and
smoothly switch to a new one without losing any log messages.
